package lesson2;

public class Task2 {
    public static void main(String[] args) {
        int n = 20;
        int count = 0;

        System.out.println("Натуральные числа неделящиеся на 2, 3, 5:");
        for (int i = 1; i <= n; i++){
            if (((i % 2 != 0) && (i % 3 != 0) && (i % 5 != 0)) || i == 2 || i == 3 || i == 5){
                count++;
                System.out.println(i);
            }
        }
        System.out.println("Общее количество натуральных чисел: " + count);

        // Решение для поиска простых чисел
/*
        for (int i = 1; i <= n; i++){
            int del = 0;
            for (int j = 1; j <= i; j++){
                if (i % j == 0){
                    del++;
                }
            }
            if (del <= 2){
                System.out.println(i + " - простое число");
                count++;
            }
        }
        System.out.println("Общее количество простых чисел чисел " + count);
 */
    }
}
