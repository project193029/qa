package lesson2;

public class Task4 {
    public static void main(String[] args) {
        int n = 230;
        float sum = 0;
        float col = 0;
        int compos = 1;
        float average = 0;
        float mod = 1;
        int sumSquares = 0;
        int sumCube = 0;
        int first;
        int last;
        int sumFirstLast;

        String str = Integer.toString(n);

        for (int i = 0; i < str.length(); i++) {
            int num = Character.getNumericValue(str.charAt(i));
            sum += num;
            col++;
            compos *= num;
            sumSquares += Math.pow(num, 2);
            sumCube += Math.pow(num, 3);
            mod *= 0.1;
        }

        average = sum / col;
        first = Character.getNumericValue(str.charAt(0));
        last = Character.getNumericValue(str.charAt(str.length()-1));

        sumFirstLast = first + last;

        System.out.println("Количестов цифр - " + (int)col);
        System.out.println("Сумма цифр - " + (int)sum);
        System.out.println("Произведение цифр - " + compos);
        System.out.println("Среднее арифметическое цифр - " + average);
        System.out.println("Сумма квадратов цифр - " + sumSquares);
        System.out.println("Сумма кубов цифр - " + sumCube);
        System.out.println("Первая цифра - " + first);
        System.out.println("Последняя цифра - " + last);
        System.out.println("Сумма первой и последней цифр - " + sumFirstLast);
    }
}
