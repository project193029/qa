package lesson2;

public class Task3 {
    public static void main(String[] args) {
        String str = "Тестим   лишние    пробелы     ";
        StringBuilder builder = new StringBuilder();
        if (str.charAt(0) != ' ') {
            builder.append(str.charAt(0));
        }

        for (int i = 1; i < str.length(); i++) {
            if (str.charAt(i) != ' ' || (str.charAt(i) == ' ' && str.charAt(i - 1) != ' ')) {
                builder.append(str.charAt(i));
            }
        }
        String result = builder.toString();
        String finalResult = result.substring(0, result.length());
        
        if (result.charAt(result.length() - 1) == ' '){
           finalResult = result.substring(0, result.length()-1);
        }

        System.out.println(finalResult);
    }
}
