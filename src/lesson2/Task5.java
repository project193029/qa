package lesson2;

public class Task5 {
    public static void main(String[] args) {
        String str = "Я пошла в магазин и купила 1 арбуз, 6 огурцов, 15 конфет и 100 банок пива";
        int col = 0;
        StringBuilder builder = new StringBuilder();
        int i = 0;
        String result;

        while (i < str.length()){
            builder.delete(0, builder.length());
            if (Character.isDigit(str.charAt(i))){
                while (Character.isDigit(str.charAt(i))){
                    builder.append(str.charAt(i));
                    i++;
                }
            } else {
                i++;
                continue;
            }
            col++;
            i++;
            result = builder.toString();
            System.out.println("Количество " + col + ": " + result);
        }
    }
}
