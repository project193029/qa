package lesson1;

public class Months {
    public static final String JAN = "Январь";
    public static final String FEB = "Февраль";
    public static final String MAR = "Март";
    public static final String APR = "Апрель";
    public static final String MAY = "Май";
    public static final String JUN = "Июнь";
    public static final String JUL = "Июль";
    public static final String AUG = "Август";
    public static final String SEP = "Сентябрь";
    public static final String OCT = "Октябрь";
    public static final String NOV = "Ноябрь";
    public static final String DEC = "Декабрь";
}
