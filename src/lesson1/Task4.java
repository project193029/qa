package lesson1;

public class Task4 {
    public static void main(String[] args) {
        System.out.println("Диапазон значений типа int: " + Integer.MIN_VALUE + " - " + Integer.MAX_VALUE);
        System.out.println("Диапазон значений типа long: " + Long.MIN_VALUE + " - " + Long.MAX_VALUE);
        System.out.println("Диапазон значений типа short: " + Short.MIN_VALUE + " - " + Short.MAX_VALUE);
        System.out.println("Диапазон значений типа byte: " + Byte.MIN_VALUE + " - " + Byte.MAX_VALUE);
    }
}
